<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\File;

class PackageStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            //
            'title'=>'string|required',
            'address'=>'string|required',
            'description'=>'string|required',
            'price'=>'required|numeric|gte:0',
            'discount'=>'required|numeric|min:0|max:99',
            'categories'=>['required','array','exists:categories,id'],
            'categories.*'=>['required','numeric','exists:categories,id'],
            'courses'=>['required','array','exists:courses,id'],
            'courses.*'=>['required','numeric','exists:courses,id'],
            'src'=>['required',File::image()]
        ];
    }
}
