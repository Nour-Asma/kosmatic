<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LearnerCourseRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'address'=>'required|string',
            'phone'=>'required|string',
            'city'=>'required|string',
            'email'=>'required|email',
            'postal_code'=>'required|string',
            'seats'=>'required|numeric|min:1|max:10',
            'course_id'=> ['required' , 'exists:courses,id'],
            'course_date_id'=>['required' , 'exists:course_dates,id'],
            'contract' => 'required|file|mimes:pdf',
        ];
    }
}
