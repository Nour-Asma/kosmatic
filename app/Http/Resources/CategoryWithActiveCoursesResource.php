<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\Course;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryWithActiveCoursesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $coureses_ids= Category::find($this->id)->categoryCourse->map(function ($item){
            return $item->course_id;
        });

        $coureses = Course::find($coureses_ids)->where('active',1);

        $packages_ids= Category::find($this->id)->packageCategory->map(function ($item){
            return $item->package_id;
        });

        $packages = Package::find($packages_ids)->where('active',1);

        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'created_at'=>$this->created_at,
            'packages'=> PackageBasicResource::collection($packages),
            'courses'=> CourseResource::collection($coureses)
        ];
    }
}
