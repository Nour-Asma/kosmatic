<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\Course;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CoursesWithCategoriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $priceAfterDiscount = $this->price;
        if($this->discount > 0){
            $theDiscountedAmount = ($this->discount * $this->price)/ 100;
            $priceAfterDiscount = $priceAfterDiscount - $theDiscountedAmount ;
        }

        $categories_id= Course::find($this->id)->categoryCourse->map(function ($item){
            return $item->category_id;
        });

        $categories = Category::find($categories_id);

        $packages_ids = $this->packages->map(function ($item){
            return $item->package_id;
        });

        $packages = CourseBasicResource::collection(Package::find($packages_ids));

        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'address'=>$this->address,
            'price'=>$this->price,
            'discount'=>$this->discount,
            'active'=>$this->active,
            'price_after_discount'=>$priceAfterDiscount,
            'created_at'=>$this->created_at,
            'src'=>$this->src,
            'categories'=>CategoryResource::collection($categories),
            'dates'=> CourseDateResource::collection($this->courseDate->sortBy('from_date')),
            'part_of_packages'=>PackageForCoursesResource::collection($packages),

        ];
    }
}
