<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\Course;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryWithCoursesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $coureses_ids= Category::find($this->id)->categoryCourse->map(function ($item){
            return $item->course_id;
        });

        $coureses = Course::find($coureses_ids);

        $package_ids= Category::find($this->id)->packageCategory?->map(function ($item){
            return $item->package_id;
        });

        $packages = Package::find($package_ids);

        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'created_at'=>$this->created_at,
            'courses'=> CourseResource::collection($coureses),
            'packages'=> PackageForCategory::collection($packages),

        ];
    }
}
