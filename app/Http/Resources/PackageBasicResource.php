<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\Course;
use App\Models\Package;
use App\Models\PackageDate;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PackageBasicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $priceAfterDiscount = $this->price;
        if($this->discount > 0){
            $theDiscountedAmount = ($this->discount * $this->price)/ 100;
            $priceAfterDiscount = $priceAfterDiscount - $theDiscountedAmount ;
        }
        $courses_ids = $this->courses->map(function ($item){
            return $item->course_id;
        });

        $courses = CourseBasicResource::collection(Course::find($courses_ids));

        $categories_id= $this->categories->map(function ($item){
            return $item->category_id;
        });

        $categories = Category::find($categories_id);
        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'price'=>$this->price,
            'discount'=>$this->discount,
            'address'=>$this->address,
            'description'=>$this->description,
            'active'=>$this->active,
            'price_after_discount'=>$priceAfterDiscount,
            'image'=>$this->src,
            'courses'=>$courses,
            'categories'=>CategoryResource::collection($categories),
            'dates' =>PackageDateResource::collection($this->packageDates->sortBy('from_date')),
            'created_at'=>$this->created_at,
        ];
    }
}
