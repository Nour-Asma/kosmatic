<?php

namespace App\Http\Resources;

use App\Models\Course;
use App\Models\CourseDate;
use App\Models\Learner;
use App\Models\Package;
use App\Models\PackageDate;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Date;

class RegistrationPackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $package = Package::find($this->package_id);
        $learner = Learner::find($this->learner_id);
        $date= PackageDate::find($this->package_date_id);

        return [
            'id'=>$learner->id,
            'first_name'=>$learner->first_name,
            'last_name'=>$learner->last_name,
            'email'=>$learner->email,
            'address'=>$learner->address,
            'city'=>$learner->city,
            'phone'=>$learner->phone,
            'postal_code'=>$learner->postal_code,
            'contract'=>$learner->contract,
            'status'=>$learner->status,
            'seats'=>$learner->seats,
            'package'=>[
              'id'=>$package->id,
              'title'=>$package->title
            ],
            'date'=>new PackageDateResource($date),
            'date_of_registration'=>$learner->created_at
        ];
    }
}
