<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CourseBasicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $priceAfterDiscount = $this->price;
        if($this->discount > 0){
            $theDiscountedAmount = ($this->discount * $this->price)/ 100;
            $priceAfterDiscount = $priceAfterDiscount - $theDiscountedAmount ;
        }

        return
            [
                'id'=>$this->id,
                'title'=>$this->title,
                'price'=>$this->price,
                'discount'=>$this->discount,
                'active'=>$this->active,
                'price_after_discount'=>$priceAfterDiscount,
                'address'=>$this->address,
                'image'=>$this->src,
                'created_at'=>$this->created_at,
            ];
    }
}
