<?php

namespace App\Http\Resources;

use App\Models\Course;
use App\Models\CourseDate;
use App\Models\Learner;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RegistrationCourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $course = Course::find($this->course_id);
        $learner = Learner::find($this->learner_id);
        $date = CourseDate::find($this->course_date_id);

        return [
            'id'=>$learner->id,
            'first_name'=>$learner->first_name,
            'last_name'=>$learner->last_name,
            'email'=>$learner->email,
            'address'=>$learner->address,
            'city'=>$learner->city,
            'phone'=>$learner->phone,
            'postal_code'=>$learner->postal_code,
            'contract'=>$learner->contract,
            'status'=>$learner->status,
            'seats'=>$learner->seats,
            'course'=>[
                'id'=>$course->id,
                'title'=>$course->title
            ],
            'date'=>CourseDateResource::make($date),
            'date_of_registration'=>$learner->created_at
        ];
    }
}
