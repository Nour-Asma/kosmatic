<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\Course;
use App\Models\CourseDate;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $priceAfterDiscount = $this->price;
        if($this->discount > 0){
            $theDiscountedAmount = ($this->discount * $this->price)/ 100;
            $priceAfterDiscount = $priceAfterDiscount - $theDiscountedAmount ;
        }


        return [
            'id'=>$this->id,
            'title'=>$this->title,
            'price'=>$this->price,
            'address'=>$this->address,
            'discount'=>$this->discount,
            'active'=>$this->active,
            'price_after_discount'=>$priceAfterDiscount,
            'dates'=> CourseDateResource::collection($this->courseDate->sortBy('from_date')),
            'created_at'=>$this->created_at,
            'src'=>$this->src

        ];

    }
}
