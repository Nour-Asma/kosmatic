<?php

namespace App\Http\Controllers;

use App\Http\Requests\CourseDatesRequest;
use App\Http\Requests\DatesUpdateRequest;
use App\Http\Resources\CourseDateResource;
use App\Models\CourseDate;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CourseDateController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CourseDatesRequest $request)
    {
        //
        $fields = $request->validated();


            $fromDate = Carbon::createFromFormat('d-m-Y', $fields['from_date']);
            $toDate = Carbon::createFromFormat('d-m-Y', $fields['to_date']);

            CourseDate::create([
                'from_date'=>$fromDate,
                'to_date'=>$toDate,
                'course_id'=>$fields['course_id'],
                'location'=>$fields['location'],
            ]);

        return response([
            'message'=>'created successfully',
        ],200);
    }

    /**
     * Display the specified resource.
     */
    public function show(CourseDate $courseDate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(DatesUpdateRequest $request, $id)
    {

        //
        $courseDate = CourseDate::find($id);
        if(!$courseDate){
            return response([
                'message'=>'not found'
            ],404);
        }
        $fields = $request->validated();
        $fromDate = Carbon::createFromFormat('d-m-Y', $fields['from_date']);
        $toDate = Carbon::createFromFormat('d-m-Y', $fields['to_date']);
        $courseDate->update(
            [
                'from_date'=>$fromDate,
                'to_date'=>$toDate,
                'location'=>$fields['location'],
            ]
        );
        return response([
            'message'=>'updated successfully',
            'courseDate'=>new CourseDateResource($courseDate),
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $courseDate = CourseDate::find($id);
        if(!$courseDate){
            return response([
                'message'=>'not found'
            ],404);
        }
        $courseDate->delete();
        return response([
            'message'=>'successfully deleted'
        ],200);
    }
}
