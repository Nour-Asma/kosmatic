<?php

namespace App\Http\Controllers;

use App\Models\LearnerPackage;
use Illuminate\Http\Request;

class LearnerPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public static function store($package_id , $learner_id , $package_date_id)
    {
        //
        LearnerPackage::create([
            'package_id'=>$package_id,
            'learner_id'=>$learner_id,
            'package_date_id'=>$package_date_id,
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(LearnerPackage $learnerPackage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, LearnerPackage $learnerPackage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(LearnerPackage $learnerPackage)
    {
        //
    }
}
