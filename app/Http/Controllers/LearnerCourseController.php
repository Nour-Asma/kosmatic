<?php

namespace App\Http\Controllers;

use App\Models\LearnerCourse;
use App\Models\LearnerPackage;
use Illuminate\Http\Request;

class LearnerCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */

    public static function store($course_id , $learner_id,$course_date_id)
    {
        //
        LearnerCourse::create([
            'course_id'=>$course_id,
            'learner_id'=>$learner_id,
            "course_date_id"=>$course_date_id
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(LearnerCourse $learnerCourse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, LearnerCourse $learnerCourse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(LearnerCourse $learnerCourse)
    {
        //
    }



}
