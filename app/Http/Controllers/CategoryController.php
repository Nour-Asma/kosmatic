<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CategoryWithActiveCoursesResource;
use App\Http\Resources\CategoryWithCoursesResource;
use App\Http\Resources\CourseResource;
use App\Http\Resources\PackageBasicResource;
use App\Models\Category;
use App\Models\Course;
use App\Models\Package;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $categories = Category::all();
        return response([
            'categories'=>CategoryResource::collection($categories)
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CategoryStoreRequest $request)
    {
        //
        $fields = $request->validated();
        $category = Category::create($fields);

        return response([
            'message'=>'created Successfully',
            'category'=> new CategoryResource($category)
            ],200);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $category = Category::find($id);

        if(!$category){
            return response(['message'=>'not found'],404);
        }

        return response(['category'=>CategoryWithCoursesResource::make($category)]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CategoryUpdateRequest $request,$id)
    {
        //
            $category = Category::find($id);

            if(!$category){
                return response(['message'=>'not found'],404);
            }

            $category->update($request->validated());
            return response([
                'message'=>'updated successfully',
                'category' => new CategoryResource($category)
            ],200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //
        $category = Category::find($id);

        if(!$category){
            return response(['message'=>'not found'],404);
        }
       $category->delete();
        return response(['message'=>'deleted successfully'],200);
    }

    public function showToUser($id){
        $category = Category::find($id);

        if(!$category){
            $packages = Package::all()->where('active',1);

            $coureses = Course::all()->where('active', 1);

            return response([
                'packages'=> PackageBasicResource::collection($packages),
                'courses'=> CourseResource::collection($coureses)
            ],200);
        }

        return response(['category'=>CategoryWithActiveCoursesResource::make($category)]);

    }
}
