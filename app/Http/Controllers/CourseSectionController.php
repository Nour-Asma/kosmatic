<?php

namespace App\Http\Controllers;

use App\Http\Requests\CourseSectionStoreRequest;
use App\Http\Requests\CourseSectionUpdateRequest;
use App\Http\Resources\CourseSectionResource;
use App\Models\CourseSection;
use Illuminate\Http\Request;

class CourseSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CourseSectionStoreRequest $request)
    {
        //

        $fields = $request->validated();


            CourseSection::create($fields);

        return response([
            'message'=>'created successfully',
        ],200);
    }

    /**
     * Display the specified resource.
     */
    public function show(CourseSection $courseSection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CourseSectionUpdateRequest $request, $id)
    {
        //
        $courseSection = CourseSection::find($id);
        if(!$courseSection){
            return response([
                'message'=>'not found'
            ],404);
        }

        $fields = $request->validated();

        if(isset($fields['src']) && $fields['src'] !== null){
            if($courseSection->src){
                $imagePath = public_path($courseSection->src);
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                }
            }
        }

        $courseSection->update($fields);
        return response([
            'message'=>'updated successfully',
            'courseSection'=>new CourseSectionResource($courseSection),
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //
        $courseSection = CourseSection::find($id);
        if(!$courseSection){
            return response([
                'message'=>'not found'
            ],404);
        }
        // Delete the image file from the storage
        if($courseSection->src){
        $imagePath = public_path($courseSection->src);


            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
        }
        $courseSection->delete();
        return response([
            'message'=>'successfully deleted'
        ],200);
    }
}
