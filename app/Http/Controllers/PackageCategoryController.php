<?php

namespace App\Http\Controllers;

use App\Models\PackageCategory;
use Illuminate\Http\Request;

class PackageCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $fields= $request->validate([
            'category_id'=> ['required' ,'exists:categories,id'],
            'package_id'=> ['required' ,'exists:packages,id'],
        ]);

        PackageCategory::create($fields);

        return response([
            'message'=>'successfully added'
        ],200);

    }

    /**
     * Display the specified resource.
     */
    public function show(PackageCategory $packageCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, PackageCategory $packageCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        //

        $request->validate([
            'category_id'=> ['required' ,'exists:categories,id'],
            'package_id'=> ['required' ,'exists:packages,id'],
        ]);

        $packageCategory = PackageCategory::where('category_id',$request['category_id'])
            ->where('package_id', $request['package_id'])
            ->first();

        if(!$packageCategory){
            return response([
                'message'=>'not found'
            ],404);

        }
        $packageCategory->delete();
        return response([
            'message'=>'successfully deleted'
        ],200);
    }
}
