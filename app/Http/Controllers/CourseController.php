<?php

namespace App\Http\Controllers;

use App\Http\Requests\CourseStoreRequest;
use App\Http\Requests\CourseUpdateRequest;
use App\Http\Resources\CourseResource;
use App\Http\Resources\CourseSectionResource;
use App\Http\Resources\CoursesWithCategoriesResource;
use App\Http\Resources\CourseWithRegistration;
use App\Models\Category;
use App\Models\CategoryCourse;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\File;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //

        return response([
            'courses'=> CoursesWithCategoriesResource::collection(Course::all()),
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CourseStoreRequest $request)
    {
        //
        $fields = $request->validated();

        $course = Course::create($fields);

        foreach ($fields['categories'] as $category_id){
            $categoryCourse= CategoryCourse::create([
                'course_id'=>$course->id,
                'category_id'=>$category_id
            ]);
        }

        return response([
            'message'=>'created successfully',
            'course'=> new CoursesWithCategoriesResource($course)
        ],200);

    }

    /**
     * Display the specified resource.
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CourseUpdateRequest $request, $id)
    {
        //
        $course = Course::find($id);
        $fields = $request->validated();
        if(!$course){
            return response([
                'message'=>'not found'
            ],404);
        }
        $course->update($fields);
        return response([
            'message'=>'updated successfully',
            'course'=> new CoursesWithCategoriesResource($course)
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //
        $course = Course::find($id);
        if(!$course){
            return response([
                'message'=>'not found'
            ],404);
        }
        $course->delete();
        return response([
            'message'=>'deleted successfully',
        ],200);
    }

    public function courseWithSection($id){
        $course = Course::find($id);
        if(!$course){
            return response([
                'message'=>'not found'
            ],404);
        }
        return response([
            'course'=>new CoursesWithCategoriesResource($course),
            'sections'=>CourseSectionResource::collection($course->courseSections)
        ]);
    }

    public function switchActivity($id){
        $course = Course::find($id);
        if(!$course){
            return response([
                'message'=>'not found'
            ],404);
        }
        $course->update([
            'active'=>!$course->active
        ]);
        return response([
            'message'=>'done',
        ],200);
    }

    public function onHome(){
        return response([
            'courses'=> CoursesWithCategoriesResource::collection(Course::all()
                ->where('active',1)
                ->where('on_home_page' , 1)),
        ],200);
    }

    public function courseWithRegistration($id){
        $course = Course::find($id);
        if(!$course){
            return response([
                'message'=>'not found'
            ],404);
        }

        return response(['course'=>CourseWithRegistration::make($course)],200);
    }

    public function updatePic(Request $request , $id){
        $course = Course::find($id);
        if(!$course){
            return response([
                'message'=>'not found'
            ],404);
        }

        $request->validate(['src'=>['required',File::image()]]);



        if(isset($request['src']) && $request['src'] !== null){
            if($course->src){
                $imagePath = public_path($course->src);
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                }
            }
        }
        $course->update(['src'=>$request['src']]);

        return response(['message'=>'successfully updated'],200);
    }
}
