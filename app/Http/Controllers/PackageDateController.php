<?php

namespace App\Http\Controllers;

use App\Http\Requests\CourseDatesRequest;
use App\Http\Requests\DatesUpdateRequest;
use App\Http\Requests\PackageDateRequest;
use App\Http\Resources\PackageDateResource;
use App\Models\CourseDate;
use App\Models\PackageDate;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PackageDateController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PackageDateRequest $request)
    {
        //
        $fields = $request->validated();


        $fromDate = Carbon::createFromFormat('d-m-Y', $fields['from_date']);
        $toDate = Carbon::createFromFormat('d-m-Y', $fields['to_date']);

        PackageDate::create([
            'from_date'=>$fromDate,
            'to_date'=>$toDate,
            'package_id'=>$fields['package_id'],
            'location'=>$fields['location'],
        ]);

        return response([
            'message'=>'created successfully',
        ],200);
    }


    /**
     * Display the specified resource.
     */
    public function show(PackageDate $packageDate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(DatesUpdateRequest $request, $id)
    {

        //
        $packageDate = PackageDate::find($id);
        if(!$packageDate){
            return response([
                'message'=>'not found'
            ],404);
        }
        $fields = $request->validated();
        $fromDate = Carbon::createFromFormat('d-m-Y', $fields['from_date']);
        $toDate = Carbon::createFromFormat('d-m-Y', $fields['to_date']);
        $packageDate->update(
            [
                'from_date'=>$fromDate,
                'to_date'=>$toDate,
                'location'=>$fields['location'],
            ]
        );
        return response([
            'message'=>'updated successfully',
            'courseDate'=>new PackageDateResource($packageDate),
        ],200);
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $packageDate = PackageDate::find($id);
        if(!$packageDate){
            return response([
                'message'=>'not found'
            ],404);
        }
        $packageDate->delete();
        return response([
            'message'=>'successfully deleted'
        ],200);
    }

}
