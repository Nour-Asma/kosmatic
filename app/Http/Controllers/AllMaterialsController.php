<?php

namespace App\Http\Controllers;

use App\Http\Resources\AllMaterialsResource;
use App\Http\Resources\CourseResource;
use App\Http\Resources\PackageBasicResource;
use App\Models\Course;
use App\Models\Package;
use Illuminate\Http\Request;

class AllMaterialsController extends Controller
{
    //
    public static function index (){
        $packages = Package::all()->where('active',1);

        $coureses = Course::all()->where('active', 1);

        return response([
            'packages'=> PackageBasicResource::collection($packages),
            'courses'=> CourseResource::collection($coureses)
        ],200);
    }

    public  function getDiscount(){
        $packages = Package::all()
            ->where('active',1)
            ->where('discount','>', '0');

        $coureses = Course::all()
            ->where('active', 1)
            ->where('discount','>', '0');

        return response([
            'packages'=> PackageBasicResource::collection($packages),
            'courses'=> CourseResource::collection($coureses)
        ],200);
    }
}
