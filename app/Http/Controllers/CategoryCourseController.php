<?php

namespace App\Http\Controllers;

use App\Models\CategoryCourse;
use Illuminate\Http\Request;

class CategoryCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //

       $fields= $request->validate([
            'category_id'=> ['required' ,'exists:categories,id'],
            'course_id'=> ['required' ,'exists:courses,id'],
        ]);

       CategoryCourse::create($fields);

        return response([
            'message'=>'successfully added'
        ],200);

    }

    /**
     * Display the specified resource.
     */
    public function show(CategoryCourse $categoryCourse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, CategoryCourse $categoryCourse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        //

        $request->validate([
            'category_id'=> ['required' ,'exists:categories,id'],
            'course_id'=> ['required' ,'exists:courses,id'],
        ]);

        $courseCategory = CategoryCourse::where('category_id',$request['category_id'])
            ->where('course_id', $request['course_id'])
            ->first();

        if(!$courseCategory){
            return response([
                'message'=>'not found'
            ],404);

        }
        $courseCategory->delete();
        return response([
            'message'=>'successfully deleted'
        ],200);
    }

}
