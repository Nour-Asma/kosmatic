<?php

namespace App\Http\Controllers;

use App\Http\Requests\LearnerCourseRegisterRequest;
use App\Http\Requests\LearnerPackageRegisterRequest;
use App\Http\Resources\RegistrationCourseResource;
use App\Http\Resources\RegistrationPackageResource;
use App\Mail\MailNotify;
use App\Models\Learner;
use App\Models\LearnerCourse;
use App\Models\LearnerPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class LearnerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //

        return response([
            'package_registration'=> RegistrationPackageResource::collection(LearnerPackage::all()),
            'course_registration'=> RegistrationCourseResource::collection(LearnerCourse::all()),
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function storePackage(LearnerPackageRegisterRequest $request)
    {
        //
        $fields = $request->validated();

        $learner = Learner::create($fields);

        LearnerPackageController::store($fields['package_id'], $learner->id,$fields['package_date_id']);

        return response([
            'message'=>'registered successfully',
        ],200);
    }

    public function storeCourse(LearnerCourseRegisterRequest $request)
    {
        //
        $fields = $request->validated();

        $learner = Learner::create($fields);


        LearnerCourseController::store($fields['course_id'], $learner->id , $fields['course_date_id']);

        return response([
            'message'=>'registered successfully',
        ],200);
    }


    /**
     * Display the specified resource.
     */
    public function show(Learner $learner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Learner $learner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Learner $learner)
    {
        //
    }


    public function downloadContract($id){
        $learner = Learner::find($id);
        if(!$learner){
            return response([
                'message'=>'not found'
            ],404);
        }

         $path = storage_path($learner->contract);

        return response()->download($path);
    }

    public function confirm($id){

        $learner = Learner::find($id);
        if(!$learner){
            return response([
                'message'=>'not found'
            ],404);
        }
        $learner->update(['status'=>'confirmed']);


        // Send email
        $content = 'Hello '.$learner->first_name.' '.$learner->first_name.' you\'re registration has been confirmed contact us for more details';
        try {
            Mail::to($learner->email)->send(new MailNotify('Register Confirmation' , $content));
            return response()->json(['message' => 'Email sent successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to send email: ' . $e->getMessage()], 500);
        }


        return response([
            'message'=>'successfully confirmed'
        ]);
    }

    public function reject($id){
        $learner = Learner::find($id);
        if(!$learner){
            return response([
                'message'=>'not found'
            ],404);
        }
        $learner->update(['status'=>'rejected']);


        // Send email
        $content = 'Hello '.$learner->first_name.' '.$learner->first_name.' you\'re registration has been rejected contact us for more details';
        try {
            Mail::to($learner->email)->send(new MailNotify('Register Rejection' , $content));
            return response()->json(['message' => 'Email sent successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to send email: ' . $e->getMessage()], 500);
        }

        return response([
            'message'=>'successfully rejected'
        ]);
    }


}
