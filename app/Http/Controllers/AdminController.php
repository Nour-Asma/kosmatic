<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminLoginRequest;
use App\Http\Requests\UpdateAdminRequest;
use App\Http\Resources\AdminResource;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAdminRequest $request)
    {
        //
        $fields = $request->validated();

        $request->user()->update(['password'=>bcrypt($fields['password'])]);

        return response([
            'message'=>'updated Successfully',
            'admin'=> new AdminResource($request->user()) ,
        ], 200);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Admin $admin)
    {
        //
    }

    public function login(AdminLoginRequest $request)
    {


        $fields = $request->validated();

        $user = User::where('email', $fields['email'])->first();

        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'The provided credentials are incorrect',
            ], 422);
        }
        $token = $user->createToken('token')->plainTextToken;

        $response = [
            'user' => new AdminResource($user),
            'token'=>$token,
        ];

        return response($response , 201);
    }

    public function logout(Request $request){
        $request->user()->currentAccessToken()->delete();

        return response(['message'=>'Logged out'], 200);
    }

}
