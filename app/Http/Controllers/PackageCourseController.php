<?php

namespace App\Http\Controllers;

use App\Http\Requests\PackageCourseRequest;
use App\Models\PackageCourse;
use Illuminate\Http\Request;

class PackageCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public static function store($courses_ids , $package_id)
    {
        foreach ($courses_ids as $course_id){
            PackageCourse::create([
                'package_id'=>$package_id,
                'course_id'=>$course_id
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function singleStore(Request $request)
    {
        $fields = $request->validate([
           'course_id'=>['required','exists:courses,id'],
           'package_id'=>['required','exists:packages,id']
        ]);


        PackageCourse::create($fields);
        return response([
            'message'=>'successfully'
        ],200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, PackageCourse $packageCourse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        //

        $request->validate([
            'course_id'=> ['required' ,'exists:courses,id'],
            'package_id'=> ['required' ,'exists:packages,id'],
        ]);

        $packageCourse = PackageCourse::where('course_id',$request['course_id'])
            ->where('package_id', $request['package_id'])
            ->first();

        if(!$packageCourse){
            return response([
                'message'=>'not found'
            ],404);
        }
        $packageCourse->delete();
        return response([
            'message'=>'successfully deleted'
        ],200);
    }

}
