<?php

namespace App\Http\Controllers;

use App\Http\Requests\PackageStoreRequest;
use App\Http\Requests\PackageUpdateRequest;
use App\Http\Resources\PackageBasicResource;
use App\Http\Resources\PackageForCategory;
use App\Models\Course;
use App\Models\Package;
use App\Models\PackageCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\File;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        return response([
            'packages'=> PackageForCategory::collection(Package::all()),
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PackageStoreRequest $request)
    {
        $fileds= $request->validated();
        $package = Package::create($fileds);

        foreach ($fileds['categories'] as $category_id){

            PackageCategory::create(
                [
                    'package_id'=>$package->id,
                    'category_id'=>$category_id
                ]
            );
        }

        PackageCourseController::store($fileds['courses'] , $package->id);


        return response([
            'message'=>'created successfully',
            'package'=> new PackageBasicResource($package)
        ],200);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $package = Package::find($id);
        if(!$package){
            return response([
                'message'=>'not found'
            ],404);
        }
        return  response([
            'package' => new PackageBasicResource($package)
        ],200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PackageUpdateRequest $request, $id)
    {
        $package = Package::find($id);
        if(!$package){
            return response([
                'message'=>'not found'
            ],404);
        }
        $fileds = $request->validated();

        $package->update($fileds);


        return  response([
            'message' => 'updated successfully',
            'package' => new PackageBasicResource($package)
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $package = Package::find($id);
        if(!$package){
            return response([
                'message'=>'not found'
            ],404);
        }
        $package->delete();
        return response([
            'message'=>'successfully deleted'
        ],200);
    }

    public function switchActivity($id){
        $package = Package::find($id);
        if(!$package){
            return response([
                'message'=>'not found'
            ],404);
        }
        $package->update(['active'=>!$package->active]);
        return response([
            'message'=>'switch Successfully',
            'package'=> new PackageBasicResource($package),
        ],200);
    }

    public function updatePic(Request $request , $id){
        $package = Package::find($id);
        if(!$package){
            return response([
                'message'=>'not found'
            ],404);
        }

        $request->validate(['src'=>['required',File::image()]]);



        if(isset($request['src']) && $request['src'] !== null){
            if($package->src){
                $imagePath = public_path($package->src);
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                }
            }
        }
        $package->update(['src'=>$request['src']]);

        return response(['message'=>'successfully updated'],200);
    }

}
