<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

class Learner extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function setContractAttribute($contract)
    {
        $newContractName = uniqid().''.'pdf'.'.'.$contract->extension();
        $contract->move(storage_path('app/contracts'),$newContractName);
        return $this->attributes['contract'] = '/app/contracts/'. $newContractName;
    }

    public function course(){
        return $this->hasOne(Course::class);
    }

    public function date(){
        return $this->hasOne(Date::class);
    }

    public function learnerPackage(){
        return $this->hasOne(LearnerPackage::class);
    }
    public function learnerCourse(){
        return $this->hasOne(LearnerCourse::class);
    }
}
