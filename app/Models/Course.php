<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function setSrcAttribute($src)
    {
        $newImageName = uniqid().''.'image'.'.'.$src->extension();
        $src->move(public_path('images/courses'),$newImageName);
        return $this->attributes['src'] = '/images/courses/'. $newImageName;
    }

    public function courseSections(){
        return $this->hasMany(CourseSection::class);
    }

    public function categoryCourse(){
        return $this->hasMany(CategoryCourse::class);
    }

    public function courseDate(){
        return $this->hasMany(CourseDate::class);
    }

    public function learners(){
        return $this->hasMany(LearnerCourse::class);
    }

    public function packages(){
        return $this->hasMany(PackageCourse::class);
    }

}
