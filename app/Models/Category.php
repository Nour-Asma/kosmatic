<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = ['name'];

    public function categoryCourse(){
        return $this->hasMany(CategoryCourse::class);
    }

    public function packageCategory(){
        return $this->hasMany(PackageCategory::class);
    }
}
