<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function setSrcAttribute($src)
    {
        $newImageName = uniqid().''.'image'.'.'.$src->extension();
        $src->move(public_path('images/packages'),$newImageName);
        return $this->attributes['src'] = '/images/packages/'. $newImageName;
    }
    public function courses(){
        return $this->hasMany(PackageCourse::class);
    }
    public function categories(){
        return $this->hasMany(PackageCategory::class);
    }
    public function learnerPackage(){
        return $this->hasMany(LearnerPackage::class);
    }
    public function packageDates(){
        return $this->hasMany(PackageDate::class);
    }
}
