<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseSection extends Model
{
    use HasFactory;
    protected $guarded =['id'];

    public function setSrcAttribute($src)
    {
        $newImageName = uniqid().''.'image'.'.'.$src->extension();
        $src->move(public_path('images/sections'),$newImageName);
        return $this->attributes['src'] = '/images/sections/'. $newImageName;
    }
}
