<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AllMaterialsController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CategoryCourseController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\CourseDateController;
use App\Http\Controllers\CourseSectionController;
use App\Http\Controllers\LearnerController;
use App\Http\Controllers\LearnerCourseController;
use App\Http\Controllers\LearnerPackageController;
use App\Http\Controllers\PackageCategoryController;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\PackageCourseController;
use App\Http\Controllers\PackageDateController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', [AdminController::class,'login']);


Route::group(['middleware' => ['auth:sanctum']] , function (){

    // Auth and Admin
    Route::put('admin/update',[AdminController::class,'update']);
    Route::post('logout', [AdminController::class, 'logout']);

    // Category
    Route::post('category/store', [CategoryController::class , 'store']);
    Route::put('category/update/{id}', [CategoryController::class , 'update']);
    Route::delete('category/delete/{id}', [CategoryController::class , 'destroy']);
    Route::get('admin-category',[CategoryController::class, 'index']);
    Route::get('admin-category/{id}', [CategoryController::class,'show']);

    // Courses
    Route::post('course/store',[CourseController::class,'store']);
    Route::get('admin-course',[CourseController::class,'index']);
    Route::put('course/update/{id}',[CourseController::class,'update']);
    Route::delete('course/delete/{id}',[CourseController::class, 'destroy']);
    Route::get('course-with-sections-admin/{id}' , [CourseController::class, 'courseWithSection']);
    Route::put('course-active/{id}' , [CourseController::class, 'switchActivity']);
    Route::post('course-update-image/{id}' , [CourseController::class , 'updatePic']);

    //Course Category
    Route::delete('course-category/delete' , [CategoryCourseController::class , 'destroy']);
    Route::post('course-category/store' , [CategoryCourseController::class , 'store']);



    // Course Section
    Route::post('course-sections/store',[CourseSectionController::class , 'store']);
    Route::post('course-sections/update/{id}', [CourseSectionController::class, 'update']);
    Route::delete('course-sections/delete/{id}',[CourseSectionController::class,'destroy']);

    // Course Dates
    Route::post('course-date/store',[CourseDateController::class , 'store']);
    Route::delete('course-date/delete/{id}',[CourseDateController::class,'destroy']);
    Route::patch('course-date/update/{id}',[CourseDateController::class,'update']);


    // Packages
    Route::post('package/store',[PackageController::class,'store']);
    Route::delete('package/delete/{id}',[PackageController::class,'destroy']);
    Route::patch('package/switch/{id}',[PackageController::class,'switchActivity']);
    Route::put('package/update/{id}',[PackageController::class,'update']);
    Route::get('package-admin',[PackageController::class,'index']);
    Route::get('package-admin/{id}',[PackageController::class,'show']);
    Route::post('package-update-image/{id}' , [PackageController::class , 'updatePic']);

    //Package Category
    Route::delete('package-category/delete' , [PackageCategoryController::class , 'destroy']);
    Route::post('package-category/store' , [PackageCategoryController::class , 'store']);



    // Package Dates
    Route::post('package-date/store',[PackageDateController::class , 'store']);
    Route::delete('package-date/delete/{id}',[PackageDateController::class,'destroy']);
    Route::patch('package-date/update/{id}',[PackageDateController::class,'update']);



    // Courses Packages
    Route::post('package-course/store',[PackageCourseController::class,'singleStore']);
    Route::delete('package-course/delete',[PackageCourseController::class,'destroy']);



    // Registration
    Route::get('registration',[LearnerController::class,'index']);
    Route::get('contract/{id}' , [LearnerController::class,'downloadContract']);
    // Confirmations
    Route::put('confirm-registration/{id}' , [LearnerController::class,'confirm']);
    Route::put('reject-registration/{id}' , [LearnerController::class,'reject']);



});

// Category
Route::get('category',[CategoryController::class, 'index']);
Route::get('category/{id}', [CategoryController::class,'show']);


// All Courses On Home
Route::get('course',[CourseController::class,'onHome']);

// Course Details
Route::get('course-with-sections/{id}' , [CourseController::class, 'courseWithSection']);

// Making Registration
Route::post('make-package-registration' , [LearnerController::class,'storePackage']);
Route::post('make-course-registration' , [LearnerController::class,'storeCourse']);




// Get Category Courses
Route::get('category-courses-packages/{id}', [CategoryController::class,'showToUser']);

// Get All Categories
Route::get('category',[CategoryController::class, 'index']);

// Get Package Details
Route::get('package/{id}',[PackageController::class,'show']);


// Get all packages and courses
Route::get('packages-courses',[AllMaterialsController::class,'index']);
Route::get('packages-courses-discount',[AllMaterialsController::class,'getDiscount']);
