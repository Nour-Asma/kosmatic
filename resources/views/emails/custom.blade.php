<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email Template</title>
</head>
<body style="font-family: Arial, sans-serif; background-color: #f2f2f2; margin: 0; padding: 20px;">

<div class="container" style="background-color: #ffffff; padding: 20px; border-radius: 5px; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);">
    <h1 style="color: #333333; font-size: 24px; margin-bottom: 20px;">Willkommen bei German Board | Kosmetikschule</h1>
    <p style="color: #666666; font-size: 16px; line-height: 1.6;">{!! $content !!}</p>

    <div style="margin-top: 30px;">
        <h3 style="color: #333333; font-size: 18px; margin-bottom: 10px;">Kontaktinformationen:</h3>
        <ul style="list-style: none; padding: 0; margin: 0;">
            <li style="margin-bottom: 10px;">Telefon: <a href="tel:+4917680203257">+49 176 80203257</a></li>
            <li style="margin-bottom: 10px;">Telefon: <a href="tel:+4917644466088">+49 176 44466088</a></li>
            <li style="margin-bottom: 10px;">Telefon: <a href="tel:+4915738922887">+49 157 38922887</a></li>
            <li style="margin-bottom: 10px;">Telefon: <a href="tel:+4915566223331">+49 155 66223331</a></li>
            <li style="margin-bottom: 10px;">Adresse: Kreuzeskirchstraße 8, 45127 Essen</li>
            <li style="margin-bottom: 10px;">Email: <a href="mailto:info@germanboard.org">info@germanboard.org</a></li>
            <li style="margin-bottom: 10px;">Facebook: <a href="https://www.facebook.com/germanboardkosmetik">German Board Kosmetikschule</a></li>
            <li style="margin-bottom: 10px;">Instagram: <a href="https://www.instagram.com/germanboardkosmetik">germanboardkosmetik</a></li>
            <li style="margin-bottom: 10px;">Snapchat: german_board</li>
        </ul>
    </div>
</div>

</body>
</html>
